import React, { Component } from 'react';
import Header from './components/Header';
import News from './components/News';
import JSON from './db.json';

// in es6 as of now we can import component like  the upper statements import React, { Component } from 'react';
// if we use this syntax we dont need to write like this app extends React.component

//functionL COMPONENTS ARE state less
//class components have states

//when we use arrow function we dont need to use bind function as well

export default class App extends Component {
  state = {
    keywords: '',
    todo: [],
    news: JSON,
    active: false
  };
  eventHandler = e => {
    const check = e.target.value === '' ? false : true;
    this.setState({
      active: check,
      keywords: e.target.value
    });
  };
  addItem = () => {
    // this.setState({
    //   todo
    // });
    var newArray = [...this.state.todo];
    newArray.push(this.state.keywords);
    this.setState({ todo: newArray });
  };

  render() {
    return (
      <div>
        <div>
          <h1>{this.state.keywords}</h1>
          <button onClick={this.addItem} />

          <input
            style={{ backgroundColor: `${this.state.active ? 'red' : 'blue'}` }}
            onChange={this.eventHandler}
            type="text"
          />
        </div>
        {this.state.todo}
        <News />
      </div>
    );
  }
}
